const elms = {
	player: getElement('.player'),
	video: getElement('video'),
	progressRange: getElement('.progress-range'),
	progressBar: getElement('.progress-bar'),
	playBtn: getElement('#play-btn'),
	volumeIcon: getElement('#volume-icon'),
	volumeRange: getElement('.volume-range'),
	volumeBar: getElement('.volume-bar'),
	currentTime: getElement('.time-elapsed'),
	duration: getElement('.time-duration'),
	speed: getElement('.player-speed'),
	fullscreenBtn: getElement('.fullscreen'),
};

const {
	player,
	video,
	progressRange,
	progressBar,
	playBtn,
	volumeIcon,
	volumeRange,
	volumeBar,
	currentTime,
	duration,
	speed,
	fullscreenBtn,
} = elms;

const isMobile = /Android|webOS|iPhone|iPad|iPod/i.test(navigator.userAgent);

function getElement(selector) {
	return document.querySelector(selector);
}

function setWidthStyle(elm, value) {
	return (elm.style.width = value);
}

// Play & Pause ----------------------------------- //
function showPlayIcon() {
	playBtn.classList.replace('fa-pause', 'fa-play');
	playBtn.setAttribute('title', 'Play');
}

function togglePlay() {
	if (video.paused) {
		video.play();
		playBtn.classList.replace('fa-play', 'fa-pause');
		playBtn.setAttribute('title', 'Pause');
	} else {
		video.pause();
		showPlayIcon();
	}
}

// Progress Bar ---------------------------------- //

// Calculate display time format
function displayTime(time) {
	const minutes = Math.floor(time / 60);
	let seconds = Math.floor(time % 60);
	seconds = seconds > 9 ? seconds : `0${seconds}`;
	return `${minutes}:${seconds}`;
}

// Update progress bar as the video plays
function updateProgress() {
	setWidthStyle(progressBar, `${(video.currentTime / video.duration) * 100}%`);
	currentTime.textContent = `${displayTime(video.currentTime)} /`;
	duration.textContent = `${displayTime(video.duration)}`;
}

// Click to seek within the video
function setProgress(e) {
	const newTime = e.offsetX / progressRange.offsetWidth;
	setWidthStyle(progressBar, `${newTime * 100}%`);
	video.currentTime = newTime * video.duration;
}

// Volume Controls --------------------------- //
let lastVolume = 1;
let lastIcon = 'fa-volume-up';
let isMuted = false;

// Volume bar
function changeVolume(e) {
	let volume = e.offsetX / volumeRange.offsetWidth;
	// round volume up or down
	if (volume < 0.1) {
		volume = 0;
	}
	if (volume > 0.9) {
		volume = 1;
	}
	setWidthStyle(volumeBar, `${volume * 100}%`);
	video.volume = volume;

	// change volume icon depending on volume
	volumeIcon.className = '';
	if (volume > 0.7) {
		volumeIcon.classList.add('fas', 'fa-volume-up');
		lastIcon = 'fa-volume-up';
	} else if (volume < 0.7 && volume > 0) {
		volumeIcon.classList.add('fas', 'fa-volume-down');
		lastIcon = 'fa-volume-down';
	} else if (volume === 0) {
		volumeIcon.classList.add('fas', 'fa-volume-off');
		lastIcon = 'fa-volume-off';
	}

	lastVolume = volume;
}

// Mute/unmute
function toggleMute() {
	volumeIcon.className = '';
	if (!isMuted) {
		isMuted = true;
		lastVolume = video.volume;
		video.volume = 0;
		setWidthStyle(volumeBar, 0);
		volumeIcon.classList.add('fas', 'fa-volume-mute');
		volumeIcon.setAttribute('title', 'Unmute');
	} else {
		isMuted = false;
		video.volume = lastVolume;
		setWidthStyle(volumeBar, `${lastVolume * 100}%`);
		volumeIcon.classList.add('fas', lastIcon);
		volumeIcon.setAttribute('title', 'Mute');
	}
}

// Change Playback Speed -------------------- //
function changeSpeed() {
	video.playbackRate = speed.value;
}

// Fullscreen ------------------------------- //

// View video in fullscreen
function openFullscreen(elm) {
	if (elm.requestFullscreen) {
		elm.requestFullscreen();
	} else if (elm.mozRequestFullscreen) {
		elm.mozRequestFullscreen();
	} else if (elm.webkitRequestFullscreen) {
		elm.webkitRequestFullscreen();
	} else if (elm.msRequestFullscreen) {
		elm.msRequestFullscreen();
	} else if (isMobile) {
		video.webkitEnterFullscreen();
	}
	video.classList.add('video-fullscreen');
}

// Close fullscreen
function closeFullscreen() {
	if (document.fullscreenElement) {
		document.exitFullscreen();
	} else if (document.mozExitFullscreen) {
		document.mozExitFullscreen();
	} else if (document.webkitExitFullscreen) {
		document.webkitExitFullscreen();
	} else if (document.msExitFullscreen) {
		document.msExitFullscreen();
	} else if (document.cancelFullScreen) {
		document.cancelFullScreen();
	} else if (document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
	} else if (document.webkitCancelFullScreen) {
		document.webkitCancelFullScreen();
	} else if (isMobile) {
		video.webkitExitFullScreen();
		showPlayIcon();
	}
	video.classList.remove('video-fullscreen');
}

let fullscreen = false;

// toggle fullscreen
function toggleFullscreen() {
	!fullscreen ? openFullscreen(player) : closeFullscreen();
	fullscreen = !fullscreen;
}

function escPressExit() {
	if (
		!document.fullscreenElement &&
		!document.webkitIsFullScreen &&
		!document.mozFullScreen &&
		!document.msFullscreenElement &&
		fullscreen
	) {
		toggleFullscreen();
	}
}

// Event listeners
playBtn.addEventListener('click', togglePlay);
video.addEventListener('click', togglePlay);
video.addEventListener('ended', showPlayIcon);
video.addEventListener('timeupdate', updateProgress);
video.addEventListener('canplay', updateProgress);
progressRange.addEventListener('click', setProgress);
volumeRange.addEventListener('click', changeVolume);
volumeIcon.addEventListener('click', toggleMute);
speed.addEventListener('change', changeSpeed);
fullscreenBtn.addEventListener('click', toggleFullscreen);
document.addEventListener('fullscreenchange', escPressExit);
document.addEventListener('webkitfullscreenchange', escPressExit);
document.addEventListener('mozfullscreenchange', escPressExit);
document.addEventListener('MSFullscreenChange', escPressExit);
video.addEventListener('webkitendfullscreen', escPressExit);
